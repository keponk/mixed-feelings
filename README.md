Mixed Feelings
====

Project for Mixed and Augmented Reality course at Université Paris Sud.

# Description
The goal of the course project was to think about different interactions techniques in Augmented Reality. We developed a phone application implementing the idea of a "Portal" into another dimension. A portal would allow the user to look through an area and be able to send things to and bring things from that other dimension. In other words a "window" into another dimension. Morover, we wanted to explore how big this portal could be in the real world relative to the user.

The portal mechanism was achieved by with stock unity camera renderers, whereas the positioning in the real world was done through the tracking mechanism in Vuforia.  For the real world positionning we used a 120x90cm poster in a wall. The bigger size allowed us to increase the distance between the portal and the phone. 

We wanted to explore the ammount of immersion we could provide playing with the size of the portal and 3D audio. We foudn that 3D audio played a major role captivating users attention.

Vuforia showed some limits regarding the materials used to display images, reflection being a main pain point. The big scale of the portal also had a positive feedback regarding the overall experience. A future project could involve increasing the size of the portal and have more animated characters interact with the user. The use of an HMD is expected to heavily increase the immersion.

![video_file_no_sound](res/mixedfeelings_720p.webm)

# Unity Setup
This project was developed using Unity LTS 2017.4.16f1.


1. Configure Vuforia
  * Edit > Project Settings > Player..
  * Enable Vuforia under XR Settings for both windows and android

 
2. Import Vuforia DB
  * Assets > Import Package > Custom Package...
  * Choose `test_db_1.unitypackage` and bring it all in (name may change soon)
  
3. Install "Vast Outer Space" assets from asset store.

4. Install FPS Controller from Standard Assets (everything, or to save space only the ones below)
  * Assets > Import Package > Characters
  * From "FirstPersonCharacters" everything (Audio, prefabs, scripts)
  * Add all of: CrossPlatformInput, Editor, Utility

Professor:
- Anastasia Bezarianos
- Jean-Marc Vezien