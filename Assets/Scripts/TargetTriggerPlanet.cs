﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TargetTriggerPlanet : MonoBehaviour, ITrackableEventHandler{

    private TrackableBehaviour mTrackableBehaviour;
    public GameObject targetObject;

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
        newStatus == TrackableBehaviour.Status.TRACKED ||
        newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
    }

    private void OnTrackingFound()
    {
        targetObject.SetActive(true);
    }

    // Use this for initialization
    void Start () {
        //targetObject = targetObject.GetComponent<GameObject>;
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
