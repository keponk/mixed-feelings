﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZMovement : MonoBehaviour {

    public float speed = -0.2f;
    public float accel = 0;

	
	// Update is called once per frame
	void Update () {
        Vector3 position = transform.position;
        if (position.z < -5 || position.z > 5)
        {
                speed *= -1;
        }
        position.z = position.z + speed;
        transform.position = position;
	}
}
