﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetWalkAcross : MonoBehaviour
{

    public Transform targetObjectTransform;
    public float movementSpeed = 0.1f;
    public float rotationSpeed = 10f;
    public float cloneScale = 1f;
    public GameObject prefab;
    public float distanceThreshold = 0.5f;
    private bool targetReached = false;
    private GameObject clone;
    private Vector3 initialPosition;
    private float step;
    public GameObject localAudio;
    public GameObject remoteAudio;
    private AudioClip[] audioArray;
    public float timeBetweenAudios = 5;
    private float audioCountDown;
    private int audioIndex = 0;
    private bool inMovement = true;
    private AudioClip astroComment1;
    private AudioClip astroComment2;
    private AudioClip astroComment3;
    private AudioClip astroComment4;
    private AudioClip astroComment5;
    private AudioClip astroTalk;
    private AudioClip astro1;
    private AudioClip astro2;


    private void Start()
    {
        initialPosition = transform.position;
        audioArray = Resources.LoadAll<AudioClip>("Audio/Astronaut");
        //for (int i = 0; i < audioArray.Length; i++)
        //{
        //    Debug.Log("index: " + i + "; name: " + audioArray[i].name);
        //}
        astroComment1 = audioArray[0];
        astroComment2 = audioArray[1];
        astroComment3 = audioArray[2];
        astroComment4 = audioArray[3];
        astroComment5 = audioArray[4];
        astroTalk = audioArray[1];

        remoteAudio.GetComponent<AudioSource>().clip = audioArray[0];
        playSound(7);
        audioCountDown = timeBetweenAudios;
    }

    void playSound(int audioIndex)
    {
        localAudio.GetComponent<AudioSource>().Stop();
        remoteAudio.GetComponent<AudioSource>().Stop();
        localAudio.GetComponent<AudioSource>().clip = audioArray[audioIndex];
        remoteAudio.GetComponent<AudioSource>().clip = audioArray[audioIndex];
        localAudio.GetComponent<AudioSource>().Play();
        remoteAudio.GetComponent<AudioSource>().Play();

    }
    void Update()
    {
        //Debug.Log("audio countdown= " + audioCountDown);
        audioCountDown -= Time.deltaTime;

        step = movementSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        if (audioCountDown <= 0f)
        {
            //playSound(2);
            audioIndex++;
            if (audioIndex == audioArray.Length)
            {
                audioIndex = 0;
            }
            audioCountDown = timeBetweenAudios;
        }
        if (!targetReached && !targetObjectTransform.gameObject.activeSelf)
        {
            return;
        }

        if (targetReached)
        {
            TargetReachedRoutine();
            return;
        }


        if ( inMovement)
        {
            playSound(0);
            inMovement = false;
        }
        


        Vector3 currentPositionXZ = new Vector3(transform.position.x, 0f, transform.position.z);
        Vector3 targetPositionXZ = new Vector3(targetObjectTransform.position.x, 0f, targetObjectTransform.position.z);
        float distance = Vector3.Distance(currentPositionXZ, targetPositionXZ);
        if (distance < distanceThreshold)
        {
            targetReached = true;
        }
        currentPositionXZ = Vector3.MoveTowards(currentPositionXZ, targetPositionXZ, step);
        currentPositionXZ.y = transform.position.y;

        //float speedX = Random.Range(-0.01f, 0.01f);
        //float speedY = Random.Range(-0.01f, 0.01f);
        //float speedZ = Random.Range(-0.05f, 0.01f);
        //currentPositionXZ.x += speedX;
        //currentPositionXZ.y += speedY;
        //currentPositionXZ.z += speedZ;

        //Vector3 magnitude = currentPositionXZ - targetPositionXZ;

        //float distanceX = transform.position.x + targetObjectTransform.position.x;
        //float distanceZ = transform.position.z + targetObjectTransform.position.z;

        // APPLY CHANGES TO OBJECT
        transform.position = currentPositionXZ;
    }

    private void TargetReachedRoutine()
    {
        if (clone == null)
        {
            Vector3 hooveringPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.03f);
            clone = (GameObject)Instantiate(prefab, hooveringPosition, Quaternion.identity);
            clone.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            clone.transform.SetParent(transform);
            targetObjectTransform.gameObject.SetActive(false);
            playSound(1);
        }
        Vector3 currentPositionXZ = new Vector3(transform.position.x, 0f, transform.position.z);
        Vector3 initialPositionXZ = new Vector3(initialPosition.x, 0f, initialPosition.z);

        currentPositionXZ = Vector3.MoveTowards(currentPositionXZ, initialPositionXZ, step);
        currentPositionXZ.y = transform.position.y;
        transform.position = currentPositionXZ;
    }
}
