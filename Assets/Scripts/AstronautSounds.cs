﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronautSounds : MonoBehaviour {

    private AudioClip[] audioArray;
    private float timeCounter = 0f;
    private int audioIndex = 0;
    public int timeBetweenAudios = 11;


    // Use this for initialization
    void Start () {
        audioArray = Resources.LoadAll<AudioClip>("Audio/Astronaut");
        transform.GetComponent<AudioSource>().clip = audioArray[audioIndex];
        transform.GetComponent<AudioSource>().Play();
        audioIndex++;
    }
	
	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime;
        if ((int)timeCounter % timeBetweenAudios == 0 && (int)timeCounter > 5)
        {
            transform.GetComponent<AudioSource>().Stop();
            transform.GetComponent<AudioSource>().clip = audioArray[audioIndex];
            transform.GetComponent<AudioSource>().Play();
            audioIndex = audioIndex == audioArray.Length ? 0 : audioIndex++;
        }
    }
}
