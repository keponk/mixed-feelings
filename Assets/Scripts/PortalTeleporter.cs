﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour {

    public Transform reciever;
    private Transform collided;

    private bool playerIsOverlapping = false;

    // Update is called once per frame
    void Update()
    {
        if (playerIsOverlapping)
        {
            Vector3 portalToPlayer = collided.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            // If this is true: The player has moved across the portal
            if (dotProduct < 0f)
            {
                // Teleport him!
                float rotationDiff = -Quaternion.Angle(transform.rotation, reciever.rotation);
                rotationDiff += 180;
                collided.Rotate(Vector3.up, rotationDiff);

                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                collided.position = reciever.position + positionOffset;

                playerIsOverlapping = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Enter trigger");
        //Debug.Log("object name: " + other.name);
        if (other.tag == "ARGhost")
        {
            //Debug.Log("ghost entered!");
            playerIsOverlapping = true;
            collided = other.transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "ARGhost")
        {
            playerIsOverlapping = false;
        }
    }   
}
