﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ISSController : MonoBehaviour
{

    public Transform referecePoint;
    public float degreesPerSecond;

    void Update()
    {
        transform.RotateAround(referecePoint.position, Vector3.up, degreesPerSecond * Time.deltaTime);
        transform.RotateAround(referecePoint.position, Vector3.forward, degreesPerSecond * Time.deltaTime);
    }
}
