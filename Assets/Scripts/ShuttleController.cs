﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuttleController : MonoBehaviour
{

    public GameObject referenceObject;
    public float speed = 1f;
    public float distanceThreshold = 0.1f;
    public float angle = 1f;
    private GameObject target;


    void Start()
    {
        target = referenceObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (target.activeSelf)
        {
            float currentDistance = Vector3.Distance(transform.position, target.transform.position);
            if (currentDistance <= distanceThreshold)
            {
                transform.RotateAround(target.transform.position, Vector3.up + Vector3.forward, angle);
            } else
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
            }
            

        }
    }
}
