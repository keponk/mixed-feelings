﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarSoundTracker : MonoBehaviour {

    public Transform astronaut;
    private Vector3 currentPosition;

    void Start () {
        currentPosition = new Vector3(astronaut.position.x, astronaut.position.y - 1500, astronaut.position.z);
        transform.position = currentPosition;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newPositionXZ = new Vector3(astronaut.position.x, astronaut.position.y - 1500, astronaut.position.z);
        transform.position = newPositionXZ;
	}
}
